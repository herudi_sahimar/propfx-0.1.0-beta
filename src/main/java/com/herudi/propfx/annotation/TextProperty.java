/*
 * Copyright (C) 2015 Herudi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.herudi.propfx.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author Herudi
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD})
public @interface TextProperty {
	
    /**
     * Type to INT, STRING, DATE 
     */
    public enum Type {
        INT {;

            @Override
            public String toString() {
                return "int"; 
            }
            
        },
        STRING{
            
            @Override
            public String toString() {
                return "string"; 
            }
            
        },
        DATE{
            
            @Override
            public String toString() {
                return "date"; 
            }
            
        }
    }
    
    Type type() default Type.STRING;
    
    /**
     * 
     *
     * @return value default is text
     */
    
    public String value() default "text";    
}
