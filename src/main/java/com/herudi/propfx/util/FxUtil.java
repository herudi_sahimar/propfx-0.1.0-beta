/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.herudi.propfx.util;

import com.herudi.propfx.simpleDialog.SimpleDialog;
import java.sql.Date;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;

/**
 *
 * @author Herudi
 */
public class FxUtil {

    public enum AutoCompleteMode {
        STARTS_WITH, CONTAINING,;
    }

    public <T> void autoCompleteComboBox(ComboBox<T> comboBox, AutoCompleteMode mode) {
        ObservableList<T> data = comboBox.getItems();
        comboBox.setEditable(true);
//        comboBox.getEditor().focusedProperty().addListener(observable -> {
//            if (comboBox.getSelectionModel().getSelectedIndex() < 0) {
//                comboBox.getEditor().setText(null);
//            }
//        });
        comboBox.addEventHandler(KeyEvent.KEY_PRESSED, t -> comboBox.hide());
        comboBox.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {

            private boolean moveCaretToPos = false;
            private int caretPos;

            @Override
            public void handle(KeyEvent event) {
                try {
                    if (event.getCode() == KeyCode.UP) {
                        caretPos = -1;
                        moveCaret(comboBox.getEditor().getText().length());
                        return;
                    } else if (event.getCode() == KeyCode.DOWN) {
                        if (!comboBox.isShowing()) {
                            comboBox.show();
                        }
                        caretPos = -1;
                        moveCaret(comboBox.getEditor().getText().length());
                        return;
                    } else if (event.getCode() == KeyCode.BACK_SPACE) {
                        moveCaretToPos = true;
                        caretPos = comboBox.getEditor().getCaretPosition();
                    } else if (event.getCode() == KeyCode.DELETE) {
                        moveCaretToPos = true;
                        caretPos = comboBox.getEditor().getCaretPosition();
                    }

                    if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT
                            || event.isControlDown() || event.getCode() == KeyCode.HOME
                            || event.getCode() == KeyCode.END || event.getCode() == KeyCode.TAB) {
                        return;
                    }

                    ObservableList<T> list = FXCollections.observableArrayList();
                    data.stream().forEach((aData) -> {
                        if (mode.equals(AutoCompleteMode.STARTS_WITH) && aData.toString().toLowerCase().startsWith(comboBox.getEditor().getText().toLowerCase())) {
                            list.add(aData);
                        } else if (mode.equals(AutoCompleteMode.CONTAINING) && aData.toString().toLowerCase().contains(comboBox.getEditor().getText().toLowerCase())) {
                            list.add(aData);
                        }
                    });
                    String t = comboBox.getEditor().getText();

                    comboBox.setItems(list);
                    comboBox.getEditor().setText(t);
                    if (!moveCaretToPos) {
                        caretPos = -1;
                    }
                    moveCaret(t.length());
                    if (!list.isEmpty()) {
                        comboBox.show();
                    } else {
                        comboBox.hide();
                    }
                } catch (Exception e) {
                }
            }

            private void moveCaret(int textLength) {
                if (caretPos == -1) {
                    comboBox.getEditor().positionCaret(textLength);
                } else {
                    comboBox.getEditor().positionCaret(caretPos);
                }
                moveCaretToPos = false;
            }
        });
    }

    public <T> T getComboBoxValue(ComboBox<T> comboBox) {
        if (comboBox.getSelectionModel().getSelectedIndex() < 0) {
            return null;
        } else {
            return comboBox.getItems().get(comboBox.getSelectionModel().getSelectedIndex());
        }
    }
    
    public static<T> void AutoCompleteCombobox(ComboBox<T> combo, AutoCompleteMode mode){
        FxUtil fx = new FxUtil();
        fx.autoCompleteComboBox(combo, mode);
        fx.getComboBoxValue(combo);
    }
    
    public static void TextFieldNotString(TextField... text) {
        for (TextField t : text) {
            t.setOnKeyReleased((KeyEvent event) -> {
                char[] data = t.getText().toCharArray();
                boolean valid = true;
                for (char c : data) {
                    if (!Character.isDigit(c)) {
                        valid = false;
                        break;
                    }
                }
                if (!valid) {
                    SimpleDialog.showErrorDialog(t.getId().replace("txt", "")+" Not String");
                    t.clear();
                    t.requestFocus();
                }
            });
        }
    }
    
    public static void TextFieldNotString(String errorDialog, TextField... text) {
        for (TextField t : text) {
            t.setOnKeyReleased((KeyEvent event) -> {
                char[] data = t.getText().toCharArray();
                boolean valid = true;
                for (char c : data) {
                    if (!Character.isDigit(c)) {
                        valid = false;
                        break;
                    }
                }
                if (!valid) {
                    SimpleDialog.showErrorDialog(t.getId().replace("txt", "")+" "+errorDialog);
                    t.clear();
                    t.requestFocus();
                }
            });
        }
    }
    
    public static<T> void convertMoneyColumn(TableColumn<T, Integer>... col) {
        for (TableColumn<T, Integer> table : col) {
            table.setCellFactory(new Callback<TableColumn<T, Integer>, TableCell<T, Integer>>() {
                @Override
                public TableCell<T, Integer> call(TableColumn<T, Integer> param) {
                    final TableCell<T, Integer> cell = new TableCell<T, Integer>() {
                        @Override
                        public void updateItem(Integer item, boolean empty) {
                            super.updateItem(item, empty);
                            if (!isEmpty()) {
                                Label label = new Label();
                                NumberFormat nf = NumberFormat.getCurrencyInstance();
                                String format = nf.format(item);
                                label.setText(format);
                                setGraphic(label);
                            } else {
                                setGraphic(null);
                            }
                        }
                    };
                    return cell;
                }
            });
        }
    }
    
    public static<T> void convertDateColumn(String format, TableColumn<T, Date>... col) {
        for (TableColumn<T, Date> table : col) {
            table.setCellFactory(new Callback<TableColumn<T, Date>, TableCell<T, Date>>() {
                @Override
                public TableCell<T, Date> call(TableColumn<T, Date> param) {
                    final TableCell<T, Date> cell = new TableCell<T, Date>() {
                        @Override
                        public void updateItem(Date item, boolean empty) {
                            super.updateItem(item, empty);
                            if (!isEmpty()) {
                                Label label = new Label();
                                SimpleDateFormat df = new SimpleDateFormat(format);
                                String format = df.format(item);
                                label.setText(format);
                                setGraphic(label);
                            } else {
                                setGraphic(null);
                            }
                        }
                    };
                    return cell;
                }
                
            });
        }
    }
}
