/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.herudi.propfx.simpleDialog;

import javafx.scene.control.Alert;
import javafx.stage.StageStyle;

/**
 *
 * @author Herudi
 */
public class SimpleDialog {

    public SimpleDialog() {
    }
    
    public static void showInformationDialog(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, message);
        alert.initStyle(StageStyle.UTILITY);
        alert.setTitle("Information");
        alert.showAndWait();
    }

    public static void showErrorDialog(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR, message);
        alert.initStyle(StageStyle.UTILITY);
        alert.setTitle("Error");
        alert.showAndWait();
    }

    public static void showWarningDialog(String message) {
        Alert alert = new Alert(Alert.AlertType.WARNING, message);
        alert.initStyle(StageStyle.UTILITY);
        alert.setTitle("Warning");
        alert.showAndWait();
    }

    public static void showCustomDialog(String message, String title, Alert.AlertType alertType, StageStyle stageStyle) {
        Alert alert = new Alert(alertType, message);
        alert.initStyle(stageStyle);
        alert.setTitle(title);
        alert.showAndWait();
    }
}
