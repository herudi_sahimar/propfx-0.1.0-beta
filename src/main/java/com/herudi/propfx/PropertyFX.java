/*
 * Copyright (C) 2015 Herudi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.herudi.propfx;

import com.herudi.propfx.annotation.ColumnProperty;
import com.herudi.propfx.annotation.TextProperty;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Date;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.Property;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.StageStyle;

/**
 *
 * @author Herudi
 * @param <ModelProperty>
 *
 */
public class PropertyFX<ModelProperty> {

    private ModelProperty modelObject;
    private Object owner;

    /**
     * @param owner
     */
    public PropertyFX(Object owner) {
        super();
        if (owner == null) {
            throw new RuntimeException("Owner Is Null");
        }
        this.owner = owner;
    }

    public PropertyFX(Object owner, ModelProperty backingBean) {
        this(owner);
        this.modelObject = backingBean;
    }

    public ModelProperty getModel() {
        return modelObject;
    }

    public void setNewModel(ModelProperty bean) {
        modelObject = (ModelProperty) bean;
    }

    public void setModelColumn(ModelProperty bean, TableColumn... col) {
        modelObject = (ModelProperty) bean;
        for (TableColumn col1 : col) {
            col1.setCellValueFactory(new PropertyValueFactory(col1.getId().replace("col", "").toLowerCase()));
        }
    }

    public void clearTextAll() {
        final Field[] declaredFields = owner.getClass().getDeclaredFields();
        for (Field f : declaredFields) {
            f.setAccessible(true);
            Annotation ano = f.getAnnotation(TextProperty.class);
            if (ano instanceof TextProperty) {
                TextProperty inject = (TextProperty) ano;
                try {
                    final Object findObject = f.get(owner);
                    final String propName = inject.value();
                    if (propName.equals("text")) {
                        Property<String> controller = getProperty(propName, findObject);
                        controller.setValue("");
                    }
//                    else if (propName.equals("getValue")) {
//                        Property<Date> controller = getDateProperty(propName, findObject);
//                        controller.setValue(Date.valueOf(LocalDate.now()));
//                    }
//                    
                } catch (IllegalArgumentException | IllegalAccessException ex) {
                    Logger.getLogger(PropertyFX.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void setPropertyModel(ModelProperty bean) {
        modelObject = (ModelProperty) bean;
        final Field[] declaredFields = owner.getClass().getDeclaredFields();
        for (Field f : declaredFields) {
            f.setAccessible(true);
            Annotation ano = f.getAnnotation(TextProperty.class);
            if (ano instanceof TextProperty) {
                TextProperty inject = (TextProperty) ano;
                try {
                    final Object findObject = f.get(owner);
                    final String propName = inject.value();
                    TextProperty.Type type = inject.type();
                    if (type.toString().equals("string") && propName.equals("text")) {
                        final Property<String> controller = getProperty(propName, findObject);
                        final Property<String> model = getProperty(f.getName(), modelObject);
                        model.setValue(controller.getValue());
                    } else if (type.toString().equals("int") && propName.equals("text")) {
                        final Property<String> controller = getProperty(propName, findObject);
                        final Property<Integer> model = getProperty(f.getName(), modelObject);
                        model.setValue(Integer.valueOf(controller.getValue()));
                    } else if (type.toString().equals("date") && propName.equals("getValue")) {
                        final LocalDate controller = getDateProperty(propName, findObject);
                        setDateNonPropertyModel(f.getName(), modelObject, Date.valueOf(controller));
                    }
                } catch (IllegalArgumentException | IllegalAccessException ex) {
                    Logger.getLogger(PropertyFX.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void setClickPropertyModel(ModelProperty bean) {
        modelObject = (ModelProperty) bean;
        final Field[] declaredFields = owner.getClass().getDeclaredFields();
        for (Field f : declaredFields) {
            f.setAccessible(true);
            Annotation ano = f.getAnnotation(TextProperty.class);
            if (ano instanceof TextProperty) {
                TextProperty inject = (TextProperty) ano;
                try {
                    final Object findObject = f.get(owner);
                    final String propName = inject.value();
                    TextProperty.Type type = inject.type();
                    if (type.toString().equals("string")) {
                        final Property<String> controller = getProperty(propName, findObject);
                        final Property<String> model = getProperty(f.getName(), modelObject);
                        controller.setValue(model.getValue());
                    } else if (type.toString().equals("int")) {
                        final Property<String> controller = getProperty(propName, findObject);
                        final Property<Integer> model = getProperty(f.getName(), modelObject);
                        controller.setValue(String.valueOf(model.getValue()));
                    }

                } catch (IllegalArgumentException | IllegalAccessException ex) {
                    Logger.getLogger(PropertyFX.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
    }

    private String cres(String mit) {
        if (mit.length() == 1) {
            return mit.toUpperCase();
        }
        String firstLetter = mit.substring(0, 1).toUpperCase();
        return firstLetter + mit.substring(1);
    }

    private <T extends Property<?>> T getProperty(final String propertyName, final Object fromThis) {
        try {
            final Method propertyGetter = fromThis.getClass().getMethod(propertyName.replace("txt", "").toLowerCase() + "Property");
            final T prop = (T) propertyGetter.invoke(fromThis);
            return prop;
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new RuntimeException(String.format("Not found Property %s in object of type %s, %s", propertyName, fromThis.getClass().getName(), "owner: " + owner.getClass().getName()));
        }
    }

    private <T extends Property<?>> T getPropertyColumn(final String propertyName, final Object fromThis) {
        try {
            final Method propertyGetter = fromThis.getClass().getMethod(propertyName);
            final T prop = (T) propertyGetter.invoke(fromThis);
            return prop;
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new RuntimeException(String.format("Not found Property %s in object of type %s, %s", propertyName, fromThis.getClass().getName(), "owner: " + owner.getClass().getName()));
        }
    }

    public void setModelColumnProperty(ModelProperty bean) {
        modelObject = (ModelProperty) bean;
        final Field[] declaredFields = owner.getClass().getDeclaredFields();
        for (Field f : declaredFields) {
            f.setAccessible(true);
            Annotation ano = f.getAnnotation(ColumnProperty.class);
            if (ano instanceof ColumnProperty) {
                ColumnProperty inject = (ColumnProperty) ano;
                try {
                    final Object findObject = f.get(owner);
                    final String propName = inject.value();
                    final Property<Object> controller = getPropertyColumn(propName, findObject);
                    controller.setValue(new PropertyValueFactory<>(cres(f.getName().replace("col", ""))));
                } catch (IllegalArgumentException | IllegalAccessException ex) {
                    Logger.getLogger(PropertyFX.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void setNaturalModel(ModelProperty bean) {
        modelObject = (ModelProperty) bean;
        final Field[] declaredFields = owner.getClass().getDeclaredFields();
        for (Field f : declaredFields) {
            f.setAccessible(true);
            Annotation ano = f.getAnnotation(TextProperty.class);
            if (ano instanceof TextProperty) {
                TextProperty inject = (TextProperty) ano;
                try {
                    final Object findObject = f.get(owner);
                    final String propName = inject.value();
                    TextProperty.Type type = inject.type();
                    if (type.toString().equals("string") && propName.equals("text")) {
                        final Property<String> controller = getProperty(propName, findObject);
                        setStringNonPropertyModel(f.getName(), modelObject, controller.getValue());
                    } else if (type.toString().equals("int") && propName.equals("text")) {
                        final Property<String> controller = getProperty(propName, findObject);
                        setIntegerNonPropertyModel(f.getName(), modelObject, Integer.valueOf(controller.getValue()));
                    } else if (type.toString().equals("date") && propName.equals("getValue")) {
                        final LocalDate controller = getDateProperty(propName, findObject);
                        setDateNonPropertyModel(f.getName(), modelObject, Date.valueOf(controller));
                    }
                } catch (IllegalArgumentException | IllegalAccessException ex) {
                    Logger.getLogger(PropertyFX.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    

    public void setClickModel(ModelProperty bean) {
        modelObject = (ModelProperty) bean;
        final Field[] declaredFields = owner.getClass().getDeclaredFields();
        for (Field f : declaredFields) {
            f.setAccessible(true);
            Annotation ano = f.getAnnotation(TextProperty.class);
            if (ano instanceof TextProperty) {
                TextProperty inject = (TextProperty) ano;
                try {
                    final Object findObject = f.get(owner);
                    final String propName = inject.value();
                    TextProperty.Type type = inject.type();
                    if (type.toString().equals("string") && propName.equals("text")) {
                        final Property<String> controller = getProperty(propName, findObject);
                        controller.setValue(getStringNonPropertyModel(f.getName(), modelObject));
                    } else if (type.toString().equals("int") && propName.equals("text")) {
                        final Property<String> controller = getProperty(propName, findObject);
                        controller.setValue(getIntegerNonPropertyModel(f.getName(), modelObject).toString());
                    }
//                    else if (type.toString().equals("date") && propName.equals("getValue")) {
//                        final Property<Date> controller = getClickDateProperty(propName, findObject);
//                        controller.setValue(Date.valueOf(LocalDate.now()));
//                    }
                } catch (IllegalArgumentException | IllegalAccessException ex) {
                    Logger.getLogger(PropertyFX.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

//    private <T extends Property<Date>> T getClickDateProperty(final String propertyName, final Object fromThis) {
//        try {
//            final Method propertyGetter = fromThis.getClass().getMethod(propertyName);
//            final T prop = (T) propertyGetter.invoke(fromThis);
//            return prop;
//        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
//            throw new RuntimeException(String.format("did not found a JavaFX-Property %s in object of type %s, %s",propertyName, fromThis.getClass().getName(), "owner: " + owner.getClass().getName()));
//        }
//    }
    private <T extends LocalDate> T getDateProperty(final String propertyName, final Object fromThis) {
        try {
            final Method propertyGetter = fromThis.getClass().getMethod(propertyName);
            final T prop = (T) propertyGetter.invoke(fromThis);
            return prop;
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new RuntimeException(String.format("Not found Property %s in object of type %s, %s", propertyName, fromThis.getClass().getName(), "owner: " + owner.getClass().getName()));
        }
    }

    private <T extends String> T getStringNonPropertyModel(String propertyName, Object fromThis) {
        try {
            Method propertyGetter = fromThis.getClass().getMethod("get" + propertyName.replace("txt", ""));
            return (T) propertyGetter.invoke(fromThis);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new RuntimeException(String.format("Not found Property %s in object of type %s, %s", propertyName, fromThis.getClass().getName(), "owner: " + owner.getClass().getName()));
        }
    }

    private void setStringNonPropertyModel(String propertyName, Object fromThis, String param) {
        try {
            Method propertyGetter = fromThis.getClass().getMethod("set" + propertyName.replace("txt", ""), String.class);
            propertyGetter.invoke(fromThis, param);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new RuntimeException(String.format("Not found Property %s in object of type %s, %s", propertyName, fromThis.getClass().getName(), "owner: " + owner.getClass().getName()));
        }
    }

    private <T extends Integer> T getIntegerNonPropertyModel(String propertyName, Object fromThis) {
        try {
            Method propertyGetter = fromThis.getClass().getMethod("get" + propertyName.replace("txt", ""));
            return (T) propertyGetter.invoke(fromThis);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new RuntimeException(String.format("Not found Property %s in object of type %s, %s", propertyName, fromThis.getClass().getName(), "owner: " + owner.getClass().getName()));
        }
    }

    private void setIntegerNonPropertyModel(String propertyName, Object fromThis, Integer param) {
        try {
            Method propertyGetter = fromThis.getClass().getMethod("set" + propertyName.replace("txt", ""), Integer.class);
            propertyGetter.invoke(fromThis, param);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new RuntimeException(String.format("Not found Property %s in object of type %s, %s", propertyName, fromThis.getClass().getName(), "owner: " + owner.getClass().getName()));
        }
    }

    private void setDateNonPropertyModel(String propertyName, Object fromThis, Date param) {
        try {
            Method propertyGetter = fromThis.getClass().getMethod("set" + propertyName.replace("txt", ""), Date.class);
            propertyGetter.invoke(fromThis, param);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new RuntimeException(String.format("Not found Property %s in object of type %s, %s", propertyName, fromThis.getClass().getName(), "owner: " + owner.getClass().getName()));
        }
    }

}
