/*
 * Copyright (C) 2015 Herudi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.herudi.propfx.viewFX;

import com.herudi.propfx.annotation.InjectControlView;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Herudi
 */
@InjectControlView
public class ViewFX {

    private String viewName;

    public ViewFX() {
    }

    public Parent getView() {
        try {
            setViewName(getClass().getSimpleName());
            return FXMLLoader.load(getClass().getResource(getViewName().replace("Controller", ".fxml")));
        } catch (Exception e) {
            throw new RuntimeException("Node Not Found");
        }
    }

    public Parent getViewControl() {
        try {
            setViewName(this.getClass().getSimpleName());
            String pack = "/" + this.getClass().getPackage().getName().replace(".", "/").replace("controller", "view/");
            return FXMLLoader.load(getClass().getResource(pack + getViewName().replace("Controller", ".fxml")));
        } catch (Exception e) {
            throw new RuntimeException("Node Not Found");
        }
    }

    public Parent getViewControlWithFitToParent() {
        try {
            Parent view = getViewControl();
            AnchorPane.setTopAnchor(view, 0.0);
            AnchorPane.setRightAnchor(view, 0.0);
            AnchorPane.setLeftAnchor(view, 0.0);
            AnchorPane.setBottomAnchor(view, 0.0);
            return view;
        } catch (Exception e) {
            throw new RuntimeException("Node Not Found");
        }

    }

    public Parent getViewWithFitToParent() {
        try {
            Parent view = getView();
            AnchorPane.setTopAnchor(view, 0.0);
            AnchorPane.setRightAnchor(view, 0.0);
            AnchorPane.setLeftAnchor(view, 0.0);
            AnchorPane.setBottomAnchor(view, 0.0);
            return view;
        } catch (Exception e) {
            throw new RuntimeException("Node Not Found");
        }

    }

    public String getViewName() {
        return viewName;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    public void ViewControlWithFitToParent(AnchorPane parent, Node node) {
        parent.getChildren().setAll(node);
    }

}
