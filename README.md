# README #

PropFX is a small library and small framework for JavaFX applications . function to handle textfield and tableview that many in the View.

### What is the PropFX? ###
for example
-----------------------------------------------------------------------------------------------
this manual code on method save :
-----------------------------------------------------------------------------------------------
```
#!java
@FXML
private void onSave(ActionEvent event) {
    Person p = new Person();
    p.setName(txtName.getText());
    p.setAddress(txtAddress.getText());
    p.setTown(txtTown.getText());
    p.setHoby(txtHoby.getText());
    p.setCountry(txtCountry.getText());
    listPerson.add(p);
}

```
-----------------------------------------------------------------------------------------------

this code using PropFX on method save :
-----------------------------------------------------------------------------------------------
```
#!java
//property with param object model
private final PropertyFX<Person> prop = new PropertyFX<>(this);

...
@FXML
private void onSave(ActionEvent event) {
    prop.setNaturalModel(new Person());
    listPerson.add(prop.getModel());
}

```
-----------------------------------------------------------------------------------------------


* Property Access for Javafx the Small Framework
* Version 0.1.0-BETA
* See - http://rudy-007.com/javafx-very-simple-crud-with-propfx/